<?php

// construct
$dossierRoot = rtrim(__DIR__, '/\\') . '/';
$dossierVendor = $dossierRoot.'/vendor/';
$dossierContenu = $dossierRoot.'/contenu/';
$dossierContenuCo = $dossierRoot.'/contenu_collab/';
$dossierConfig = $dossierRoot.'/config/';


// init
require_once $dossierVendor.'autoload.php';
include('core/functions.php');


// run
echo note_run();
