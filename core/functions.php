<?php

use Symfony\Component\Yaml\Yaml;


function note_run() {
  global $dossierContenu;
  global $dossierConfig;
  global $dossierContenuCo;

  // création des pages
  $GLOBALS["pages"] = structure($dossierContenu);

  $GLOBALS["pages2"] = structure2($dossierContenu);

  // $GLOBALS["pages_collab"] = structure_collab($dossierContenuCo);

  // init des parsers
  $GLOBALS["MyParsedown"] = new MyParsedown();

  $yaml = Yaml::parse(file_get_contents($dossierConfig.'/config.yaml'));
  $GLOBALS["titre"] = $yaml['titre'];
  for($i = 0; $i <= 3; $i++) {
      $GLOBALS["categories"][$i] = $yaml['categories'][$i];
  }

  $requestUrl = evaluateRequestUrl();
  return dirige_url($requestUrl);
}



function evaluateRequestUrl() {
  // voir evaluateRequestUrl de Pico
  $scriptName = $_SERVER['SCRIPT_NAME'];
  $basePath = dirname($scriptName);
  $basePathLength = strlen($basePath);

  $requestUri = $_SERVER['REQUEST_URI'];
  if ($requestUri && (substr($requestUri, 0, $basePathLength) === $basePath)) {
      $requestUri = substr($requestUri, $basePathLength);
  }

  $requestUrl = "";
  if ($requestUri && ($requestUri !== basename($scriptName))) {
      $requestUrl = rtrim(rawurldecode($requestUri), '/');
  }
  return $requestUrl;
}

function dirige_url($url) {
  global $dossierRoot;

  $is_admin = verif_url($url);  
  if ($is_admin) {
    include($dossierRoot.'admin/index.php');
  } else {
    // $val = file_get_contents($rootDir.'templates/home.php');
    include($dossierRoot.'templates/home.php');
    // return $val;
  }
}

function verif_url($url) {
  switch($url) {
    case '/ecrire':
      return true;
      break;
    case '':
      return false;
      break;
  }
}

// url de base
function getBaseUrl() {
    $host = 'localhost';
    if (!empty($_SERVER['HTTP_X_FORWARDED_HOST'])) {
        $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
    } elseif (!empty($_SERVER['HTTP_HOST'])) {
        $host = $_SERVER['HTTP_HOST'];
    } elseif (!empty($_SERVER['SERVER_NAME'])) {
        $host = $_SERVER['SERVER_NAME'];
    }

    $port = 80;
    if (!empty($_SERVER['HTTP_X_FORWARDED_PORT'])) {
        $port = (int) $_SERVER['HTTP_X_FORWARDED_PORT'];
    } elseif (!empty($_SERVER['SERVER_PORT'])) {
        $port = (int) $_SERVER['SERVER_PORT'];
    }

    $hostPortPosition = ($host[0] === '[') ? strpos($host, ':', strrpos($host, ']') ?: 0) : strrpos($host, ':');
    if ($hostPortPosition !== false) {
        $port = (int) substr($host, $hostPortPosition + 1);
        $host = substr($host, 0, $hostPortPosition);
    }

    $protocol = 'http';
    if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
        $secureProxyHeader = strtolower(current(explode(',', $_SERVER['HTTP_X_FORWARDED_PROTO'])));
        $protocol = in_array($secureProxyHeader, array('https', 'on', 'ssl', '1'), true) ? 'https' : 'http';
    } elseif (!empty($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off')) {
        $protocol = 'https';
    } elseif ($port === 443) {
        $protocol = 'https';
    }

    $basePath = isset($_SERVER['SCRIPT_NAME']) ? dirname($_SERVER['SCRIPT_NAME']) : '/';
    $basePath = !in_array($basePath, array('.', '/', '\\'), true) ? $basePath . '/' : '/';

    if ((($protocol === 'http') && ($port !== 80)) || (($protocol === 'https') && ($port !== 443))) {
        $host = $host . ':' . $port;
    }

    return $protocol . "://" . $host . $basePath;
}


/* IFRAME MEDIA */

function sendCURL($url) {
  $ch = curl_init($url);
  $timeout = 4.5;
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
  $result = curl_exec($ch);

  curl_close($ch);
  return $result;
}

function blockAudio($text) {
  preg_match("/\(audio: (.+)\)/", $text, $matches);
  if (!empty($matches)) {
    $tag = "<audio controls src='$matches[1]'></audio>";
    $text = preg_replace("/\(audio: (.+)\)/", $tag, $text);
  }
  return $text;
}
function blockVideo($text) {
  preg_match("/\(video: (.+)\)/", $text, $matches);
  if (!empty($matches)) {
    $tag = "<video controls src='$matches[1]'></video>";
    $text = preg_replace("/\(video: (.+)\)/", $tag, $text);
  }
  return $text;
}

function blockVimeo($text) {
  preg_match("/\(vimeo: (.+)\)/", $text, $str);
  if (!empty($str)) {
    $oembedURL = "http://vimeo.com/api/oembed.json?url=".urlencode($str[1]); 
    $data = sendCURL($oembedURL);
    $data = json_decode($data, true);
    if(is_array($data) && isset($data['html'])) {
      $iframe = $data['html'];
    }
    $text = preg_replace("/\(vimeo: (.+)\)/", $iframe, $text);
  }
  return $text;
}

function blockYoutube($text) {
  preg_match("/\(youtube: (.+)\)/", $text, $str);
  if (!empty($str)) {
    $oembedURL = "http://www.youtube.com/oembed?url=".urlencode($str[1]); 
    $data = sendCURL($oembedURL);
    $data = json_decode($data, true);
    if(is_array($data) && isset($data['html'])) {
      $iframe = $data['html'];
    }
    
    $text = preg_replace("/\(youtube: (.+)\)/", $iframe, $text);
  }
  return $text;
}

function blockSoundcloud($text) {
  preg_match("/\(soundcloud: (.+)\)/", $text, $str);
  if (!empty($str)) {
    $oembedURL = "https://soundcloud.com/oembed?url=".urlencode($str[1])."&format=json"; 
    
    $data = sendCURL($oembedURL);
    // print_r($data);
    $data = json_decode($data, true);
    if(is_array($data) && isset($data['html'])) {
      $iframe = $data['html'];
    }
    $text = preg_replace("/\(soundcloud: (.+)\)/", $iframe, $text);
  }
  return $text;
}

function makeResponsive($out) {
  $out = str_ireplace('<iframe ', "<div class='TextformatterVideoEmbed' style='position:relative;padding:30px 0 56.25% 0;height:0;overflow:hidden;'><iframe ", $out);
  $out = str_ireplace('<iframe ', "<iframe style='position:absolute;top:0;left:0;width:100%;height:100%;' ", $out);
  $out = str_ireplace('</iframe>', "</iframe></div>", $out);
  return $out;
}


class MyParsedown extends ParsedownExtra
  {

      function text($text)
      {
          // integration pseudo code
          $text = blockAudio($text);
          $text = blockVideo($text);
          $text = blockVimeo($text);
          $text = blockYoutube($text);
          $text = blockSoundcloud($text);
          $text = makeResponsive($text);

          $markup = parent::text($text);

          # merge consecutive dl elements

          $markup = preg_replace('/<\/dl>\s+<dl>\s+/', '', $markup);

          # add footnotes

          if (isset($this->DefinitionData['Footnote']))
          {
              $Element = $this->buildFootnoteElement();

              $markup .= "\n" . $this->element($Element);
          }

          return $markup;
      }
      


      protected function element(array $Element)
      {
         if (preg_match('/[0-9]/', $Element['name'])) {
            $markup = '<'.$Element['name'].' data-sort="'.clean($Element['text']).'"';
         } else {
            $markup = '<'.$Element['name'];
         }

        if (isset($Element['attributes']))
        {
            foreach ($Element['attributes'] as $name => $value)
            {
                if ($value === null)
                {
                    continue;
                }

                $markup .= ' '.$name.'="'.$value.'"';
            }
        }

        if (isset($Element['text']))
        {
            $markup .= '>';

            if (isset($Element['handler']))
            {
                $markup .= $this->{$Element['handler']}($Element['text']);
            }
            else
            {
                $markup .= $Element['text'];
            }

            $markup .= '</'.$Element['name'].'>';
        }
        else
        {
            $markup .= ' />';
        }

        return $markup;
      }
}



function structure($dir) {
   $result = array();
   $cdir = scandir($dir);
   foreach ($cdir as $key => $value) {
      if (!in_array($value,array(".",".."))) {

         if (is_dir($dir.DIRECTORY_SEPARATOR.$value)) {
            $current_path = $dir.DIRECTORY_SEPARATOR.$value;
            $result[$value] =  $current_path.DIRECTORY_SEPARATOR."note.md";
         } else {
            // ajoute les fichiers
            // $result[] = $value;
         }

      }
   }
   ksort($result);
   return $result;
}

function structure2($dir) {
  $ordre_file = $dir."ordre.json";
  $ordre = json_decode(file_get_contents($ordre_file));

  $result = array();
  foreach ($ordre as $key => $subdir) {
    $meta = file_get_contents($dir.$subdir."/meta.json");
    $data = json_decode($meta);
    if ($data->{'type'} === "pad") {
      $url = $data->{'url'};
    } else {
      $url = "";
    }
    $object = (object) [
      // 'path' => $dir.$subdir."/note.md",
      'path' => $dir.$subdir,
      'key' => $subdir,
      'titre' => $data->{'titre'},
      'type' => $data->{'type'},
      'url' => $url,
    ];
    $result[] = $object;
  }

  return $result;
  /*
  $result = array();
  $cdir = scandir($dir);
  foreach ($cdir as $key => $value) {
      if (!in_array($value,array(".",".."))) {

         if (is_dir($dir.DIRECTORY_SEPARATOR.$value)) {
            $current_path = $dir.DIRECTORY_SEPARATOR.$value;
            $result[$value] =  $current_path.DIRECTORY_SEPARATOR."note.md";
         } else {
            // ajoute les fichiers
            // $result[] = $value;
         }

      }
   }
   ksort($result);
   return $result;
   */
}

function structure_collab($dir) {
  $result = array();
  $cdir = scandir($dir);
  foreach ($cdir as $key => $value) {
    if (!in_array($value,array(".",".."))) {
        if (is_dir($dir.$value)) {
            $current_path = $dir.$value;
            $file =  $current_path."/meta.json";
            $data = json_decode(file_get_contents($file));
            $url = $data->url;
            $object = (object) [
              'path' => $current_path,
              'url' => $url,
            ];
            $result[] = $object;
        } else {
          // ajoute les fichiers
          // $result[] = $value;
        }

      }
  }
  return $result;
}

/* get contents d'un pad */
function get_contents($url, $title) {
  global $MyParsedown;
  $headers = get_headers($url);
  $status = substr($headers[0], 9, 3);
  if ($status == '200') {
    $contenu = $MyParsedown->text(file_get_contents($url));

    $strEnter[0] = "<h1";
    $strExit[0] = '<article class="note selected"><div class="meta">'.$title.'</div><h1';
    $strEnter[1] = "</h4>";
    $strExit[1] = "</h4></article>";
  
    $contenu = str_replace($strEnter, $strExit, $contenu);

    return $contenu;
  }
    // return file_get_contents("backup/essai2.txt");
}


function generate_tags($HTML) {
   $DOM = new DOMDocument();
   libxml_use_internal_errors(true);
   $DOM->preserveWhiteSpace = false;
   $DOM->formatOutput = true;
   $DOM->loadHTML('<?xml encoding="utf-8" ?>' . $HTML);
   libxml_clear_errors();

   $h1 = [];
   $h2 = [];
   $h3 = [];
   $h4 = [];
   $tags = [];

   foreach ($DOM->getElementsByTagName('*') as $element) {

      if ($element->tagName == 'h1') {
         $h1[] = strip_tags($DOM->saveHTML($element));
      } else if ($element->tagName == 'h2') {
         $h2[] = strip_tags($DOM->saveHTML($element));
      } else if ($element->tagName == 'h3') {
         $h3[] = strip_tags($DOM->saveHTML($element));
      } else if ($element->tagName == 'h4') {
         $h4[] = strip_tags($DOM->saveHTML($element));
      }
   }
   /*
   if (!empty(array_filter($h1))) {
      $tags[] = $h1;
   } else if (!empty(array_filter($h2))) {
      $tags[] = $h2;
   } else if (!empty(array_filter($h3))) {
      $tags[] = $h3;
   } else if (!empty(array_filter($h4))) {
      $tags[] = $h4;
   }
   */

   $tags = [$h1, $h2, $h3, $h4];

   return $tags;
}

function clean($string) {
   $string = str_replace(' ', '-', $string);
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
   $string = strtolower($string);
   return preg_replace('/-+/', '-', $string);
}

function array_flattener($a) {
   $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($a));
   $new_a = [];
   foreach($it as $v) $new_a[] = $v;
   return array_unique($new_a);
}

function files_in_dir($url) {
  $result = array();
  $cdir = scandir($url);
  foreach ($cdir as $key => $value) {
    if (!in_array($value,array(".",".."))) {

         if (is_dir($url.DIRECTORY_SEPARATOR.$value)) {
            // $current_path = $dir.DIRECTORY_SEPARATOR.$value;
            // $result[$value] =  $current_path.DIRECTORY_SEPARATOR."note.md";
         } else {
            // ajoute les fichiers
            $result[] = $value;
         }

      }
  }
  // ksort($result);
  return $result;
}