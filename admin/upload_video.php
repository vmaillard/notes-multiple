<?php

// Varibale d'erreur par soucis de lisibilité
// Evite d'imbriquer trop de if/else, on pourrait aisément s'en passer
$error = false;
$file_exists = false;

// On définis nos constantes
$newName = uniqid(true) . "_" . str_shuffle(implode(range("e", "q")));
$path = "../uploads_video";
$legalExtensions = array("MP4", "mp4");
$legalSize = "10000000"; // 10000000 Octets = 10 MO

// On récupères les infos
$file = $_FILES;
// print_r($file);
$actualName = $file[0]['name'];
$name = pathinfo($actualName)['filename'];
$tempName = $file[0]['tmp_name'];
$actualSize = $file[0]['size'];
$extension = pathinfo($actualName, PATHINFO_EXTENSION);

// On s'assure que le fichier n'est pas vide
if ($actualSize == 0) {
    $error = true;
}
// On vérifie qu'un fichier portant le même nom n'est pas présent sur le serveur
if (file_exists($path.'/'.$name.'.'.$extension)) {
    $error = true;
    $file_exists = true;
}

$tableau_noms = [];

// On effectue nos vérifications réglementaires
if (!$error) {
    if ($actualSize < $legalSize) {
        if (in_array($extension, $legalExtensions)) {
            move_uploaded_file($tempName, $path.'/'.$name.'.'.$extension);

            array_push($tableau_noms, $name.'.'.$extension);
            echo json_encode($tableau_noms);
        }
    }
} else {
    
    if ($file_exists) {
		echo "Un fichier du même nom existe déjà.";
    } else {
    	// On supprime le fichier du serveur
    	unlink($path.'/'.$name.'.'.$extension);
    
    	echo "Une erreur s'est produite";
    }
}
