<?php

// foreach ($pages as $key => $page)

$key = file_get_contents('php://input');
$file_path = "../contenu/".$key."/note.md";
$contenu = file_get_contents($file_path);

$meta = file_get_contents("../contenu/".$key."/meta.json");
$metadata = json_decode($meta);
$titre = $metadata->{'titre'};
$auteur = $metadata->{'auteur'};
if(property_exists($metadata, 'path')) {
	$path = $metadata->{'path'};
}
$metadata->{'contenu'} = $contenu;
echo json_encode($metadata);