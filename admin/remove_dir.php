<?php
// https://stackoverflow.com/questions/3349753/delete-directory-with-files-in-it

$key = file_get_contents('php://input');
$dir = "../contenu/".$key;
$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
$files = new RecursiveIteratorIterator($it,
             RecursiveIteratorIterator::CHILD_FIRST);
foreach($files as $file) {
    if ($file->isDir()){
        rmdir($file->getRealPath());
    } else {
        unlink($file->getRealPath());
    }
}
echo $dir;

rmdir($dir);