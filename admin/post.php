<?php

$value = file_get_contents('php://input');
$data = json_decode($value);

$type = $data->{'type'};
$key = $data->{'key'};
$titre = $data->{'titre'};
$auteur = $data->{'auteur'};
$contenu = $data->{'contenu'};

$meta = (object) [
	'type' => $type,
	'key' => $key,
	'titre' => $titre,
	'auteur' => $auteur,
];

$meta = json_encode($meta);

if (is_dir("../contenu/".$key)) {
	file_put_contents("../contenu/".$key."/meta.json", $meta);
	file_put_contents("../contenu/".$key."/note.md", $contenu);
} else {
	mkdir("../contenu/".$key);   
	file_put_contents("../contenu/".$key."/meta.json", $meta);
	file_put_contents("../contenu/".$key."/note.md", $contenu);
}

echo $meta;
// $path = '../'.$data->{'path'}.'/fichier.txt';

// file_put_contents($path, htmlspecialchars($data->{'value'}));
// file_put_contents($path, $data->{'value'});
// file_put_contents($path, $value);