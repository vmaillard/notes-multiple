
document.addEventListener("DOMContentLoaded",  function() {
	init();
}, false);

function init() {

    // var pour contenu non sauvegardé 
    attachedAlert = false;
    
    ScreenSize();

    var plus = document.querySelector(".plus");
    plus.addEventListener("click", createNote);
    var plus_pad = document.querySelector(".plus_pad");
    plus_pad.addEventListener("click", createPad);
    
    var upload_file = document.querySelectorAll(".upload_form [type=file]");
    for (var i = 0; i < upload_file.length; i++) {
        upload_file[i].addEventListener("input", eventDelegation);
    }
    var upload = document.querySelectorAll(".upload_form");
    for (var i = 0; i < upload.length; i++) {
        upload[i].addEventListener("submit", uploadFile);
    }

    // var upload = document.querySelector(".upload_audio");
    // upload.addEventListener("submit", uploadSon);

    // var upload = document.querySelector("#vid_list form");
    // upload.addEventListener("submit", uploadVid);


    var liste_pages = document.querySelector('#liste_pages');
    new Sortable(liste_pages, {
        handle: '.handler',
        animation: 150,
        ghostClass: 'blue-background-class',
        onEnd: function () {
            newOrder();
        }
    });

    clickSurNote();
    
    // init nouvelle note
    createNote();

    // sur écrans mobiles
    var menu_button = document.querySelectorAll(".menu_button");
    for (var i = 0; i < menu_button.length; i++) menu_button[i].addEventListener("click", toggleMenu);
    plus.addEventListener("click", toggleMenu);
}





/* ORDER */




/* PADS */

function createPad(data, key) {
    

    var clone = document.querySelector(".pad_template").content.cloneNode(true);
    var main = document.querySelector("main");
    var note = document.querySelector("main .note");

    if (note) note.remove();
    
    if (!data || data.type == "click") {
        // console.log("INIT");
        var notes = document.querySelectorAll("#liste_pages li");
        var toute_keys = [];
        for (var i = 0; i < notes.length; i++) {
            var key = notes[i].getAttribute("data-key");
            toute_keys.push(key);
        }
        // console.log(toute_keys);
        var max_key = toute_keys.length ? Array.max(toute_keys) : 0;

        clone.querySelector(".note").setAttribute("data-key", (max_key + 1));
        var already_active = document.querySelector("#liste_pages span.active");
        if(already_active) already_active.classList.remove("active");
    }

    // si click sur une note
    if (data && data.type !== "click") {
        // console.log("CLICK");

        // console.log(data);
        // console.log(key);
        
        var obj = JSON.parse(data);
        clone.querySelector(".titre").value = obj.titre;
        clone.querySelector(".auteur").value = obj.auteur;
        clone.querySelector(".url").value = obj.url;
        clone.querySelector(".note").setAttribute("data-key", key);
        clone.querySelector("iframe").src = obj.url;
    }

    var save = clone.querySelector(".save");
    save.addEventListener("click", postPad);
    var suppr = clone.querySelector(".suppr");
    suppr.addEventListener("click", removeNote);
    
    main.appendChild(clone);
}

function postPad() {
    var note = closest(this, function(el) { return el.classList.contains('note'); });

    var key = note.getAttribute("data-key");
    var titre = note.querySelector(".titre").value;
    var auteur = note.querySelector(".auteur").value;
    var url = note.querySelector(".url").value;
    // var contenu = note.querySelector("textarea").value;

    var message = {};
    message["type"] = "pad";
    message["key"] = key;
    message["titre"] = titre;
    message["auteur"] = auteur;
    message["url"] = url;
    // message["contenu"] = contenu;

    var string = JSON.stringify(message);
    fetch('../admin/post_pad.php', { method: 'POST', body: string })
        .then(function(response) { 
            return response.text();
        })
        .then(function(text) {
          console.log(text);
        })

    addToList(message);
    newOrder();
}




/* NOTES */

function clickSurNote() {
    var li = document.querySelectorAll("#liste_pages li");
    for (var i = 0; i < li.length; i++) {
        li[i].addEventListener("click", chargeNote);
    }
}

function chargeNote() {
    var french = "Des données saisies pourraient ne pas être enregistrées. Continuer tout de même ?";
    var english = "Some changes may have not be saved. Continue anyway?"
    var r = (attachedAlert) ? confirm(english) : true;

    if (r == true) {
        attachedAlert = false;
        forgetUnsave();

        var already_active = document.querySelector("#liste_pages li.active");
        if(already_active) already_active.classList.remove("active");
        this.classList.add("active");

        var key = this.getAttribute("data-key");
        
        var test_pad = this.querySelector("span").classList.contains("pad");
            fetch('../admin/read_note.php', { method: 'POST', body: key })
            .then(function(response) { 
                return response.text();
            })
            .then(function(text) {
                // console.log(text);
                if (test_pad) {
                    createPad(text, key);
                } else {
                    createNote(text, key);
                }
                
            })
    }
    
}

function removeNote() {
    var note = closest(this, function(el) {
        return el.classList.contains('note');
    });

    var data_key = note.getAttribute("data-key");
    var li = document.querySelector('#liste_pages li[data-key="'+data_key+'"]');
    if (li) li.remove();
    fetch('../admin/remove_dir.php', { method: 'POST', body: data_key })
        .then(function(response) { 
           return response.text();
        })
        .then(function(text) {
            console.log(text);
        })

    note.remove();
    newOrder();

    createNote();
}

function createNote(data, key) {

    var r = (attachedAlert) ? confirm("Des données saisies pourraient ne pas être enregistrées. Continuer tout de même ?") : true;

    if (r == true) {
        attachedAlert = false;
        forgetUnsave();

        var main = document.querySelector("main");
        var note = document.querySelector("main .note");

        if (note) note.remove();

        var clone = document.querySelector(".note_template").content.cloneNode(true);
        
        // si init
        if (!data || data.type == "click") {
            // console.log("INIT");
            var notes = document.querySelectorAll("#liste_pages li");
            var toute_keys = [];
            for (var i = 0; i < notes.length; i++) {
                var key = notes[i].getAttribute("data-key");
                toute_keys.push(key);
            }
            // console.log(toute_keys);
            var max_key = toute_keys.length ? Array.max(toute_keys) : 0;

            clone.querySelector(".note").setAttribute("data-key", (max_key + 1));
            var already_active = document.querySelector("#liste_pages span.active");
            if(already_active) already_active.classList.remove("active");
        }

        // si click sur une note
        if (data && data.type !== "click") {
            // console.log("CLICK");
            var obj = JSON.parse(data);
            clone.querySelector(".titre").value = obj.titre;
            clone.querySelector(".auteur").value = obj.auteur;
            clone.querySelector("textarea").innerHTML = obj.contenu;
            clone.querySelector(".note").setAttribute("data-key", key);

            toggleMenu();
        }

        /* EDITOR */
        var editor = clone.querySelector("textarea");
        var easyMDE = new EasyMDE({
            element: editor,
            forceSync: true,
            status: false,
            showIcons: ['strikethrough', 'code', 'table', 'redo', 'heading', 'undo', 'clean-block', 'horizontal-rule'],
            spellChecker: false
        });
        easyMDE.codemirror.on("change", function() {
            listenUnsave();
            // if (easyMDE.value().length === 0) forgetUnsave();
        });
        // EasyMDE.togglePreview();

        var save = clone.querySelector(".save");
        save.addEventListener("click", postNote);

        var suppr = clone.querySelector(".suppr");
        suppr.addEventListener("click", removeNote);

        main.appendChild(clone);
    }
}

function postNote() {
    var note = closest(this, function(el) { return el.className === 'note'; });

    var key = note.getAttribute("data-key");
    var titre = note.querySelector(".titre").value;
    var auteur = note.querySelector(".auteur").value;
    var contenu = note.querySelector("textarea").value;

    var message = {};
    message["type"] = "local";
    message["key"] = key;
    message["titre"] = titre;
    message["auteur"] = auteur;
    message["contenu"] = contenu;

    var string = JSON.stringify(message);
    fetch('../admin/post.php', { method: 'POST', body: string })
        .then(function(response) { 
            return response.text();
        })
        .then(function(text) {
          console.log(text);
        })

    addToList(message);
    newOrder();
    forgetUnsave();
}




/* PAD */




/* UPLOAD IMAGES */

function eventDelegation() {
    var inputs = document.querySelectorAll(".selectedFile");
    var index = [...inputs].indexOf(this);
    console.log(index);
    var event = new Event('submit', {cancelable: true});
    var form = document.querySelectorAll('.upload_form');
    form[index].dispatchEvent(event);
}

function uploadFile(e) {
    e.preventDefault();
    
    var div_upload = closest(this, function(el) { return el.classList.contains('upload'); });
    var file_nature = div_upload.id;
    console.log(file_nature);
    
    var inputs = document.querySelectorAll(".selectedFile");
    var index = [...inputs].indexOf(this.querySelector("[type=file]"));
    var files = inputs[index].files;

    if (file_nature == "img_list") {
        var php_file = '../admin/upload.php';
    } else if (file_nature == "snd_list") {
        var php_file = '../admin/upload_audio.php';
    } else {
        var php_file = '../admin/upload_video.php';
    }
    
    console.log(files);
    const formData = new FormData();

    for (let i = 0; i < files.length; i++) {
        let file = files[i];

        formData.append(i, file);
    }

    var note = document.querySelector("main .note");
    var key = note.getAttribute("data-key");

    formData.append("key", key);
    
    fetch(php_file, { method: 'POST', body: formData })
        .then(function(response) { 
            return response.text();
        })
        .then(function(text) {
            console.log(text);
            addFileToList(file_nature, JSON.parse(text));
            // console.log(text);
        })

}





/* LISTES : ORDRE ET AJOUT D'ELEMENTS */

function newOrder() {
    var main = document.querySelector("#liste_pages");
    var notes = main.querySelectorAll("li");
    var order = [];
    for (var i = 0; i < notes.length; i++) {
        var key = notes[i].getAttribute("data-key");
        order.push(key);
        // update ordre front-end
        // notes[i].setAttribute("data-key", i);
    }

    order = JSON.stringify(order);
    console.log(order);
    // console.log(order);

    
    fetch('../admin/write_order.php', { method: 'POST', body: order })
    .then(function(response) { 
        return response.text();
    })
    .then(function(text) {
        console.log(text);
    })
    
}

function addToList(data) {
    var liste_pages = document.querySelector("#liste_pages");
    var all_li = liste_pages.querySelectorAll("li");
    
    // check si le fichier existe déjà
    var test_li = document.querySelector('#liste_pages li[data-key="'+data.key+'"]');
    if (test_li == null) {

        var clone = document.querySelector(".li_template").content.cloneNode(true);
        var li = clone.querySelector("li");
        var span = clone.querySelector("span");
        li.setAttribute("data-key", data.key);
        var titre = (data.titre.length) ? data.titre : "<em>Untitled</em>";
        span.innerHTML = titre;

        liste_pages.appendChild(clone);
    } else {
        console.log(test_li);
    }

    clickSurNote();
}

function addFileToList(file_nature, array) {

    for (var i = 0; i < array.length; i++) {

        var files_container = document.querySelector("#"+file_nature);

        if (file_nature == "img_list") {
            var clone = setupNewImg(files_container, array[i]);
        } else if (file_nature == "snd_list") {
            var clone = setupNewSnd(files_container, array[i]);
        } else {
            var clone = setupNewVid(files_container, array[i]);
        }
        files_container.appendChild(clone);
    }
}


function setupNewImg(files_container, file) {
    var clone = document.querySelector(".img_template").content.cloneNode(true);
    var p = clone.querySelector("p");
    var img = clone.querySelector("img");

    var base_url = files_container.getAttribute("data-url");
    var upload_dir = "../uploads/";

    p.innerHTML = base_url + "uploads/" + file;
    img.src = upload_dir + file;
    return clone; 
}


function setupNewSnd(files_container, file) {
    var clone = document.querySelector(".snd_template").content.cloneNode(true);
    var p = clone.querySelector("p");
    var audio = clone.querySelector("audio");

    var base_url = files_container.getAttribute("data-url");
    var upload_dir = "../uploads_audio/";

    p.innerHTML = base_url + "uploads/" + file;
    audio.src = upload_dir + file;
    return clone; 
}

function setupNewVid(files_container, file) {
    var clone = document.querySelector(".vid_template").content.cloneNode(true);
    var p = clone.querySelector("p");
    var video = clone.querySelector("video");

    var base_url = files_container.getAttribute("data-url");
    var upload_dir = "../uploads_video/";

    p.innerHTML = base_url + "uploads/" + file;
    video.src = upload_dir + file;
    return clone; 
}


/* INTERACTION */

function toggleMenu() {
    var aside = document.querySelector("aside");
    aside.classList.toggle("visible");
}








/* MODULES */

function displayPopup(e) {
    e.preventDefault();
    console.log("hello");
}

function listenUnsave() {
    attachedAlert = true;
    window.addEventListener("beforeunload", displayPopup);
}
function forgetUnsave() {
    attachedAlert = false;    
    window.removeEventListener("beforeunload", displayPopup);
}

Array.max = function( array ){
    return Math.max.apply( Math, array );
};

var closest = function(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function ScreenSize(){
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];

    var w2 = window,
    d2 = document,
    e2 = d2.documentElement,
    g2 = d2.getElementsByTagName('body')[0];
    
    screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
    screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
}






































function editing() {
    var nodes = document.querySelectorAll(".editor");
    var target = this.parentElement;

    var index = [].indexOf.call(nodes, target);

    var toolbar = document.querySelectorAll(".editor-toolbar")[index];
    toolbar.classList.toggle("visible");

    console.log(index);
    if (!simplemde[index].isPreviewActive()) {
        this.innerHTML = "Éditer";
    } else {
        this.innerHTML = "Fermer";
    }

    simplemde[index].togglePreview();
}








/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];
var el = document.querySelectorAll('mon_sélecteur [data-attr="'+value+'"]');



****** DÉCLARATION DE FONCTION ******

function maFonction() {
	console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
	console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
	console.log("vrai");
}

if (condition1) {
	console.log("condition1 : vrai");
} else if (condition2) {
	console.log("condition2 : vrai");
} else {
	console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut",  "valeur_attribut");
el.getAttribute("nom_attribut");  




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA FENETRE ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended",  "maFonction()");
video.setAttribute("controls",  "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
	console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/




