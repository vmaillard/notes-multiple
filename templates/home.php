<?php
global $titre;
global $categories;
global $MyParsedown;
global $pages;
global $pages2;
global $pages_collab;
?>

<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="fr">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">

	<title><?= $titre ?></title>

	<link rel="stylesheet" href="templates/assets/style.css" type="text/css"/>
	<link rel="stylesheet" href="templates/assets/theme.css" type="text/css"/>

	<script type="text/javascript" src="templates/assets/node_modules/masonry-layout/dist/masonry.pkgd.min.js" defer></script>
	<script type="text/javascript" src="templates/assets/node_modules/imagesloaded/imagesloaded.pkgd.min.js" defer></script>
	<script type="text/javascript" src="templates/assets/node_modules/fontfaceobserver/fontfaceobserver.js" defer></script>
	<script type="text/javascript" src="templates/assets/javascript.js" defer></script>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

</head>
<body>

<?php

$vimeo = "https://vimeo.com/api/oembed.json?url=https://player.vimeo.com/video/302279413";
$sound = "https://soundcloud.com/oembed?url=https%3A%2F%2Fsoundcloud.com%2Fforss%2Fflickermood&format=json";


?>

<button class='menu_button fas fa-bars'></button>

<main>

	<p class="sorry">Aucun résultat</p>

<?php
$tags_collection = [];
?>


<?php foreach ($pages2 as $key => $page): ?>


<?php if($page->type === "local"): ?>

<?php
$contenu = file_get_contents($page->path."/note.md");
$meta = json_decode(file_get_contents($page->path."/meta.json"));
$html_contenu = $MyParsedown->text($contenu);
?>
	<article class="note selected">

<?php if(strlen($meta->{'titre'}) > 0 || strlen($meta->{'auteur'}) > 0): ?>
		<div class="meta">
<?php if(strlen($meta->{'titre'}) > 0): ?>
			<span class="meta_titre"><?= $meta->{'titre'} ?></span>
<?php endif; ?>
<?php if(strlen($meta->{'auteur'}) > 0): ?>
			<span class="meta_auteur"><?= $meta->{'auteur'} ?></span>
<?php endif; ?>
		</div>
<?php endif; ?>

		<?= $html_contenu ?>
	</article>

<?php
$tags = generate_tags($html_contenu);

// print_r($tags);
// if (!empty(array_filter($tags))) {
// 	$tags_collection[] = $tags;
// }
// print_r($tags);
$tags_collection[] = $tags;
?>

<?php else: ?>

	<?php $contenu_pad = get_contents($page->url.'/export/txt', $page->titre); ?>
	<?= $contenu_pad; ?>

<?php 
$tags = generate_tags($contenu_pad);
$tags_collection[] = $tags;
?>
<?php endif; ?>




<?php endforeach ?>


</main>

<aside>
	<div class="sticky">

		<button class='menu_button fas fa-bars'></button>

		<div id="menu">
			<a href="<?=getBaseUrl()?>">Parcourir</a>
			<a href="<?=getBaseUrl()?>ecrire/">Ecrire</a>	
		</div>


<?php
$h1 = [];
$h2 = [];
$h3 = [];
$h4 = [];
foreach ($tags_collection as $key => $array) {
	$h1[] = $array[0];
	$h2[] = $array[1];
	$h3[] = $array[2];
	$h4[] = $array[3];
}
?>

		<div id="tags">

			<details>
				<summary>
					<h2><?= $categories[0] ?></h2>
				</summary>

				<div class="liste_tags">
				<?php foreach(array_flattener($h1) as $tag): ?>
					<span data-sort="<?= clean($tag) ?>"><?= $tag ?></span>
				<?php endforeach; ?>
				</div>
			</details>


			<details>
				<summary>
					<h2><?= $categories[1] ?></h2>
				</summary>

				<div class="liste_tags">
				<?php foreach(array_flattener($h2) as $tag): ?>
					<span data-sort="<?= clean($tag) ?>"><?= $tag ?></span>
				<?php endforeach; ?>
				</div>
			</details>

			<details>
				<summary>
					<h2><?= $categories[2] ?></h2>
				</summary>

				<div class="liste_tags">
				<?php foreach(array_flattener($h3) as $tag): ?>
					<span data-sort="<?= clean($tag) ?>"><?= $tag ?></span>
				<?php endforeach; ?>
				</div>
			</details>

			<details>
				<summary>
					<h2><?= $categories[3] ?></h2>
				</summary>

				<div class="liste_tags">
				<?php foreach(array_flattener($h4) as $tag): ?>
					<span data-sort="<?= clean($tag) ?>"><?= $tag ?></span>
				<?php endforeach; ?>
				</div>
			</details>


		</div>

	</div>
</aside>

</body>
</html>
